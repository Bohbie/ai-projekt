﻿#pragma strict
public var walls:Texture2D;
public var floor:Texture2D;
public var mapWidth:int;
public var mapHeight:int;
public var mrguy:GameObject;
public var mrsecondguy:GameObject;
//public var tmpReplay:Replay; 
public var frames:int;
private var influenceMap:Tile[];
private var influenceDelta:float[];
private var peeDelta:float[];
private var hungerDelta:float[];
private var homeDelta:float[];
private var hotspots: Vector2[];
private var fire: boolean[];
private var decay:float;
private var sheep:int;
private var homeSpots:int[];
private var foodSpots:int[];
private var peeSpots:int[];
private var clicked:boolean;

public var onFire:boolean; 
function Start () {
	
	clicked = false;
	
	frames=0;
	sheep = GetComponent(FlockingGroup).sheep.length;
	mapWidth = walls.width;
	mapHeight = walls.height;
	//Debug.Log("map" +mapHeight);
	//Debug.Log("map" +mapWidth);
	influenceMap = new Tile[mapWidth*mapHeight];
	influenceDelta = new float[mapWidth*mapHeight];
	hungerDelta = new float[mapWidth*mapHeight];
	peeDelta = new float[mapWidth*mapHeight];
	homeDelta = new float[mapWidth*mapHeight];
	//tmpReplay = GetComponent(Replay);
	
	for(var x=0;x<mapWidth;x++){
		for(var y=0;y<mapHeight;y++){
		//Debug.Log(walls.GetPixel(x,y).grayscale);
				var neighbors = new Array();
				for(var u=-1;u<=1;u++){
					for(var v =-1;v<=1;v++){
						if(!(u==0 && v==0) && checkValid(x+u,y+v)){
							if(Mathf.Abs(v)==Mathf.Abs(u)){
								neighbors.Push(new Vector2((x+u)+((y+v)*mapWidth),1.41));
							}else{
								neighbors.Push(new Vector2((x+u)+((y+v)*mapWidth),1));
							}
							
						}
					}
				}
				var w:Vector2[] = neighbors.ToBuiltin(Vector2) as Vector2[];
			if(walls.GetPixel(x,y).grayscale>0){
				influenceMap[x+y*mapWidth] = new Tile(x+y*mapWidth,mapWidth,mapHeight,w);
			}else{
				influenceMap[x+y*mapWidth] = new Tile(0,w);
			}
		}
	}
	fire = new boolean[mapHeight*mapWidth];
	hotspots = new Vector2[2];
	hotspots[0] = new Vector2(Mathf.Floor(mrguy.transform.position.x),Mathf.Floor(mrguy.transform.position.z));
	//Debug.Log(Mathf.Floor(mrguy.transform.position.x)+","+Mathf.Floor(mrguy.transform.position.z));
	hotspots[1] = new Vector2(Mathf.Floor(mrsecondguy.transform.position.x),Mathf.Floor(mrsecondguy.transform.position.z));
	foodSpots = new int[3];
	foodSpots[0] = 52 + 55*mapWidth;
	foodSpots[1] = 81 + 46*mapWidth;
	foodSpots[2] = 232 + 58*mapWidth;
	peeSpots = new int[2];
	peeSpots[0] = 148 + 45*mapWidth;
	peeSpots[1] = 165 + 29*mapWidth;
	homeSpots = new int[4];
	homeSpots[0] = 38 + 119*mapWidth;
	homeSpots[1] = 112 + 97*mapWidth;
	homeSpots[2] = 248 + 97*mapWidth;
	homeSpots[3] = 237 + 17*mapWidth;
	
	decay=0.1;
	onFire = false;
}

function Update () {

	var v3OrgMouse:Vector3;

	if(Input.GetMouseButtonDown(0)){
		onFire = true;
	    var x:float = Input.mousePosition.x;
	    var y:float = Input.mousePosition.y;
	    Debug.Log(x + " " + y);
	    v3OrgMouse = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 100);
	    v3OrgMouse = Camera.main.ScreenToWorldPoint (v3OrgMouse);
	    //v3OrgMouse.y = transform.position.y;
	    Debug.Log(v3OrgMouse.x + " " + v3OrgMouse.y + " " + v3OrgMouse.z);
		fire[Mathf.Floor(v3OrgMouse.x)+Mathf.Floor(v3OrgMouse.z)*mapWidth] = true;
		Debug.Log(fire[Mathf.Floor(v3OrgMouse.x)+Mathf.Floor(v3OrgMouse.y)*mapWidth]);
		clicked = true;
	}
	
	if (Input.GetKey ("n")){
		onFire = false;
		for(var FIRE = 0; FIRE <fire.length ;FIRE++){
			fire[FIRE] = false;
		}
	}
	
	//var influence:float = 0.0;
	hotspots[0] = new Vector2(Mathf.Floor(mrguy.transform.position.x),Mathf.Floor(mrguy.transform.position.z));
	hotspots[1] = new Vector2(Mathf.Floor(mrsecondguy.transform.position.x),Mathf.Floor(mrsecondguy.transform.position.z));
	for(var h=0; h<hotspots.length;h++){
		//Debug.Log("Hotspot!");
		influenceMap[hotspots[h].x+hotspots[h].y*mapWidth].setInfluenceValue(1.0);
	}
	
	for(var f=0;f<fire.length;f++){
		if(fire[f]){
			influenceMap[f].setInfluenceValue(-1.0);
		}
	}
	for(var fo=0;fo<foodSpots.length;fo++){
		influenceMap[foodSpots[fo]].setHungerValue(1.0);
	}
	for(var ho=0;ho<homeSpots.length;ho++){
		influenceMap[homeSpots[ho]].setHomeValue(1.0);
	}
	for(var p=0;p<peeSpots.length;p++){
		influenceMap[peeSpots[p]].setPeeValue(1.0);
	}
	//Debug.Log("heromanguy "+(hotspots.x+hotspots.y*mapHeight));
	//var neighbours:Vector2[];
	for(var i=0;i<mapHeight*mapWidth;i++){
		//Debug.Log(i);
		if(influenceMap[i].isTile()){
			getDecay(i);
		}
	}
		//Debug.Log("res::::"+Mathf.Lerp(influenceMap[3591].influenceValue,getDecay(3591),0.5));
	for(var k=0;k<mapHeight*mapWidth;k++){
		if(influenceMap[k].isTile()){
			influenceMap[k].setInfluenceValue(influenceDelta[k]);
			if(frames<250){
				influenceMap[k].setPeeValue(peeDelta[k]);
				influenceMap[k].setHungerValue(hungerDelta[k]);
				influenceMap[k].setHomeValue(homeDelta[k]);
			}
			if(onFire){
				if(influenceMap[k].influenceValue<0){
					if(influenceMap[k].influenceValue<-0.9 && Random.value>0.99){
						fire[k] = true;
					}
					floor.SetPixel(k%mapWidth,k/mapWidth,new Color(1.0,1.0+influenceMap[k].influenceValue,1.0+influenceMap[k].influenceValue,1));
				}else{
					floor.SetPixel(k%mapWidth,k/mapWidth,new Color(1.0-influenceMap[k].influenceValue,1.0-influenceMap[k].influenceValue,1.0,1));
				}
			}else{
				floor.SetPixel(k%mapWidth,k/mapWidth,new Color(1.0-influenceMap[k].peeValue,1.0-influenceMap[k].hungerValue,1.0-influenceMap[k].homeValue,1));
			}
			
			
		}

		//
		//var pix = floor.GetPixels32();
		/*
		var tex = new Texture2D(floor.width, floor.height);
		tex.SetPixels32(pix);
		tex.Apply();
		
		tmpReplay.replay.push(tex);
		
		*/
	}
	floor.Apply();
//	Debug.Log(frames);
	frames++;

}
function getDecay(pos:int){
	var n:Vector2[] = influenceMap[pos].getNeighbours();
	var maxinf:float = 0.0;
	var mininf:float = 0.0;
	var maxpee:float = 0.0;
	var maxhunger:float = 0.0;
	var maxhome:float = 0.0;
	//for(var s=0;s<sheep;s++){
	//	minhome[s] = 0.0;
	//	maxhome[s] = 0.0;
	//}
	var inf:float;
	var hunger:float;
	var pee:float;
	var home:float;
	for(var i = 0;i<n.length;i++){
		//Debug.Log("granne "+n[i]);
		//Debug.Log("granne_inf "+influenceMap[n[i]].influenceValue);
		inf = influenceMap[n[i].x].influenceValue * Mathf.Exp(-decay*n[i].y);
		if(frames<250){
			hunger = influenceMap[n[i].x].hungerValue * Mathf.Exp(-decay*n[i].y);
			maxhunger = Mathf.Max(hunger,maxhunger);
			
			pee = influenceMap[n[i].x].peeValue * Mathf.Exp(-decay*n[i].y);
			maxpee = Mathf.Max(pee,maxpee);
			
		
			home = influenceMap[n[i].x].homeValue * Mathf.Exp(-decay*n[i].y);
			maxhome = Mathf.Max(home,maxhome);
			//}
			
		}
		maxinf = Mathf.Max(inf,maxinf);
		mininf = Mathf.Min(inf,mininf);
		
	}
	if(frames<250){
		hungerDelta[pos] = Mathf.Lerp(influenceMap[pos].hungerValue,maxhunger,0.5);
		peeDelta[pos] = Mathf.Lerp(influenceMap[pos].peeValue,maxpee,0.5);
		homeDelta[pos] = Mathf.Lerp(influenceMap[pos].homeValue,maxhome,0.5);
	}
	if(Mathf.Abs(maxinf)>Mathf.Abs(mininf)){
		influenceDelta[pos] = Mathf.Lerp(influenceMap[pos].influenceValue,maxinf,1.0);
	}else{
		influenceDelta[pos] = Mathf.Lerp(influenceMap[pos].influenceValue,mininf,0.09);
	}
	
}
function getInfluence(pos:int):Tile{

	return influenceMap[pos];

}
function checkValid(x:int,y:int):boolean{
	if(x<0 || x>=mapWidth || y<0 || y>=mapHeight || walls.GetPixel(x,y).grayscale<0.5){
		return false;
	}else{
		//Debug.Log("sant");
		return true;
	}
}
function setHomespots(spots:Vector3[]){
	homeSpots = new int[spots.length];
	for(var i=0;i<spots.length;i++){
		homeSpots[i] = spots[i].x + spots[i].y*mapWidth;
	}
}
