﻿#pragma strict
public class Tile{

	public var position:Vector2;
	public var arrayPosition:int;
	public var neighbours:Vector2[];
	public var real:boolean;
	public var influenceValue: float;
	public var hungerValue: float;
	public var peeValue: float;
	public var homeValue: float;
	
	public function Tile(pos:int, w:int, h:int, walls:Vector2[]){
		position.x = pos % h;
		position.y = pos / h;	
		arrayPosition = pos;
		real = true;
		neighbours = walls;
		//homeValue = new float[nrOfSheep];				
	}
	public function Tile(r:int,walls:Vector2[]){
		real = false;
		neighbours = walls;
		influenceValue = -1.0;
		//homeValue = new float[nrOfSheep];
	}
	public function isTile():boolean{
		return real;
	}
	public function getPosition():Vector2{
		return position;
	}
	
	public function getArrayPostion():int{
		return arrayPosition;
	}
	
	public function setInfluenceValue(val:float){
		influenceValue = val;
	}
	public function setPeeValue( val:float){
		peeValue = val;
	}
	public function setHungerValue( val:float){
		hungerValue = val;
	}
	public function setHomeValue( val:float){
		homeValue = val;
	}
	
	public function getNeighbours():Vector2[]{
		return neighbours;
	}
	 
}