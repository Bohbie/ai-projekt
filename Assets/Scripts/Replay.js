﻿#pragma strict

public var itsReplayTime:boolean;
public var replay:Array;
private var deltaT:float;
private var currentTime:float;
private var currentTextureTime:float;
private var currentTexture:int;
public var sheeps : GameObject[];
private var swap:boolean;
var startpos:Vector3;

var style : GUIStyle;

//var cameera:GameObject;

function Start () {
	sheeps = GameObject.FindGameObjectsWithTag("Sheep");
	itsReplayTime = false;
	replay = new Array();
	deltaT = 0.01;
	swap = false;
	currentTime = 0.0;
	currentTextureTime = 0.0;
	currentTexture = 0;
	//cameera = GameObject.Find("MainCamera");
	//startpos = camera.transform.position;
	
	style.normal.textColor = Color.white;
	style.fontSize = 70;
	style.fontStyle = FontStyle.BoldAndItalic;
			
}

function Update () {
	
	//start the replay
	if (Input.GetKey ("r")){
		itsReplayTime = true;
	}
	
	//removes all the sheeps
	if(itsReplayTime){
		//cameera.transform.position = Vector3(0,0,0);
		for(var i = 0; i<sheeps.length; i++){
			Destroy(sheeps[i]);
		}
		
	}
	
	//loops the replay
	if(itsReplayTime && (currentTexture < replay.length)){
		var tmpReplay:Texture2D[] = replay.ToBuiltin(Texture2D) as Texture2D[];
	
		if(Time.time - currentTime > deltaT){
			currentTime = Time.time;
			var pix = tmpReplay[currentTexture].GetPixels32();
			
			GetComponent(influence).floor.SetPixels32(pix);
			GetComponent(influence).floor.Apply();
		
			currentTexture++;
		}	
	}
	else
		currentTexture = 0;

}

//the R
function OnGUI () {

	if(itsReplayTime){
		var dT:float = 1.0;
		if(Time.time > (currentTextureTime + dT)){
				currentTextureTime = Time.time;
				swap = !swap;
		}
		if(swap)
			GUI.Label (Rect (Screen.width - 150, 40, 300, 300), "R", style);
	}
}
