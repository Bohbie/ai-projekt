﻿#pragma strict
var map:influence;
var v:Vector2;
var hungerweight:float;
var peeweight:float;
function Start () {
	v = new Vector2(0.0,0.0);
	hungerweight = Random.value; 
	peeweight = Random.value;
}

function Update () {
	var pos:int = Mathf.Floor(transform.position.x) + Mathf.Floor(transform.position.z)*map.mapWidth;
	//Debug.Log(map.onFire);
	var tile:Tile = map.getInfluence(pos);
	var inf:float;
	if(map.onFire){
		inf = tile.influenceValue;
	}else{
		var havetopee:float;
		var hungry:float;
		
		if(tile.peeValue>0.9){
			peeweight=Mathf.Max(0.0,peeweight-0.1);
		}
		if(tile.hungerValue>0.9){
			hungerweight=Mathf.Max(0.0,hungerweight-0.1);
		}
		if(peeweight>0.4){
			havetopee = peeweight;
		}else{
			havetopee = 0.0;
		}
		if(hungerweight>0.4){
			hungry = hungerweight;
		}else{
			hungry = 0.0;
		}
		var backtowork:float = Mathf.Max(0.0,1-hungerweight-peeweight);
		inf = tile.peeValue*havetopee+tile.hungerValue*hungry+tile.homeValue*backtowork;
	}
	
	
	var neighbours:Vector2[] = tile.getNeighbours();
	var bestNeighbour:int = -1;
	//Debug.Log(tile.isTile());
	for(var i=0;i<neighbours.length;i++){
			
			//Debug.Log("Bättre!");
		if(map.onFire){
			if(inf<map.getInfluence(neighbours[i].x).influenceValue){
			
			bestNeighbour = neighbours[i].x;
			inf = map.getInfluence(neighbours[i].x).influenceValue;
			}
		}else{
			if(inf<(map.getInfluence(neighbours[i].x).peeValue*havetopee+map.getInfluence(neighbours[i].x).hungerValue*hungry+map.getInfluence(neighbours[i].x).homeValue*backtowork)){
			//Debug.Log("Bättre!");
			bestNeighbour = neighbours[i].x;
			inf = map.getInfluence(neighbours[i].x).peeValue*havetopee+map.getInfluence(neighbours[i].x).hungerValue*hungry+map.getInfluence(neighbours[i].x).homeValue*backtowork;
			}
		}
		
	}
	
	if(bestNeighbour>-1){
			/*Debug.Log("Engubbe");
			Debug.Log(bestNeighbour);
			Debug.Log(tile.peeValue);
			Debug.Log(map.getInfluence(bestNeighbour).peeValue);
			Debug.Log(tile.hungerValue);
			Debug.Log(map.getInfluence(bestNeighbour).hungerValue);*/
		v = new Vector2(map.getInfluence(bestNeighbour).getPosition().x-map.getInfluence(pos).getPosition().x
		,map.getInfluence(bestNeighbour).getPosition().y-map.getInfluence(pos).getPosition().y);
	}else{
		v = new Vector2(0.0,0.0);
	}
	hungerweight = Mathf.Min(1.0,hungerweight+0.0001);
	peeweight = Mathf.Min(1.0,peeweight+0.0001);
}
function getInfluenceDirection():Vector2{
	return v;
}
function isTile(){
	var pos:int = Mathf.Floor(transform.position.x) + Mathf.Floor(transform.position.z)*map.mapWidth;
	var tile:Tile = map.getInfluence(pos);
	return tile.isTile();
}
function isDead():boolean{
	//Debug.Log("go");
	var pos:int = Mathf.Floor(transform.position.x) + Mathf.Floor(transform.position.z)*map.mapWidth;
	if(map.getInfluence(pos).influenceValue<-0.99){
		return true;
	}else{
		//Debug.Log("not ded");
		return false;
	}
}