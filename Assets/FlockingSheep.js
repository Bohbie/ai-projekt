#pragma strict
var herd:FlockingGroup;
var velocity:float;
var speed:Vector3;
var scream : AudioSource;
var cohesionweight:float;
var separationweight:float;
var alignmentweight:float;
var rotationSpeed:float;
var distance:float;
var distanceToHero:float;
var startpos:Vector3;
var randSpeed:float;

function Start () {
	speed = new Vector3(0,0,0);
	velocity = 0.05;
	
	randSpeed = Mathf.Max(Random.value * 8,4.0);
	
	//default 
	if(cohesionweight == 0.0 || separationweight == 0 || alignmentweight == 0){
		cohesionweight = 0.4;
		separationweight = 0.7;
		alignmentweight = 0.4;
	}
	
	transform.position += new Vector3(Random.value,0,Random.value*2);
	startpos = transform.position;
	rotationSpeed = 5.0;
	distance = 1.0;
	distanceToHero = 15;
}

function Update () {
	transform.position += speed*velocity;
	//Debug.Log(speed.magnitude);
	if(speed.magnitude > 1){
		animation.CrossFade("walk");
	}
	else if(speed.magnitude > 2){
		animation.CrossFade("run");
	}
	else{
		animation.CrossFade("idle");
	}
	
	
	if (speed != Vector3.zero) {
	    transform.rotation = Quaternion.Slerp(
	        transform.rotation,
	        Quaternion.LookRotation(speed),
	        Time.deltaTime * rotationSpeed
	    );
	}
	if(GetComponent(InfluencePathing).isDead()){
		die();
	}
}
function setCourse(){	
	//near hero?
	/*if((Vector3.Distance(transform.position, herd.getHero().transform.position) < distanceToHero) && (Vector3.Distance(transform.position, herd.getHero().transform.position) > 2) ){
		var hero = herd.getHero();
		speed = hero.transform.position - transform.position;
	}
	else	
		speed =  new Vector3(0.0,0.0,0);
		
		speed.Normalize();*/
		var newDir:Vector2 = GetComponent(InfluencePathing).getInfluenceDirection();
		speed = new Vector3(newDir.x,0.0,newDir.y);
		speed.Normalize();
		//Debug.Log(speed);
}
function calcAlignment(): Vector3{
	
	var pos: Vector3 = new Vector3(0,0,0);
	var neighbors: int = 0;
	var s = herd.getSheep();
	
	for(var i : int = 0; i < s.Length; i++){
		if(gameObject != s[i]){
			if(Vector3.Distance(transform.position,s[i].transform.position) < distance){
				pos.x += s[i].GetComponent(FlockingSheep).getSpeed().x;
				pos.z += s[i].GetComponent(FlockingSheep).getSpeed().z;
				neighbors++;
			}
		}
	}
	
	if(neighbors > 0){
		pos.x/=neighbors;
		pos.z/=neighbors;
		pos.Normalize();
	}

		
	return pos;
	
}
function calcCohesion(): Vector3{

	var pos: Vector3 = new Vector3(0,0,0);
	var neighbors: int = 0;
	var s = herd.getSheep();
	
	for(var i : int = 0; i < s.Length; i++){
		if(gameObject != s[i]){
			if(Vector3.Distance(transform.position,s[i].transform.position) < distance){
				pos.x += s[i].transform.position.x;
				pos.z += s[i].transform.position.z;
				neighbors++;
			}
		}
	}
	if(neighbors > 0){
		pos.x /= neighbors;
		pos.z /= neighbors;
		var new_pos: Vector3 = new Vector3(pos.x - transform.position.x, 0,  pos.z - transform.position.z);
		pos.Normalize();
		
		return new_pos;
	}
	else
 		return pos;
	
}

function calcSeparation(): Vector3{
	var pos: Vector3 = new Vector3(0,0,0);
	var neighbors: int = 0;
	var s = herd.getSheep();
	
	for(var i : int = 0; i < s.Length; i++){
		if(gameObject != s[i]){
			if(Vector3.Distance(transform.position,s[i].transform.position) < distance){			
				pos.x += (s[i].transform.position.x - transform.position.x);
				pos.z += (s[i].transform.position.z - transform.position.z);
				neighbors++;
			}
		}
	}
	if(neighbors > 0){
		pos.x /= neighbors;
		pos.z /= neighbors; 
		
		pos.x *= -1;
		pos.z *= -1;
		
		pos.Normalize();
	}
	
	return pos;
	
}
//returns speed
function getSpeed():Vector3{
	return speed;
}
//adds the weight from other boids to the current one
function addFlockWeight(){
	//if(Vector3.Distance(transform.position, herd.getHero().transform.position) < distanceToHero){
		var align: Vector3 = calcAlignment();
		var cohesion : Vector3 = calcCohesion();
		var separation : Vector3 = calcSeparation();
		speed += (align*alignmentweight) +(cohesion* cohesionweight) + (separation*separationweight);
		speed.Normalize();
		speed*=randSpeed;
	//}
}
function dontGetStuckInWallsYouIdiot(){
	if(!GetComponent(InfluencePathing).isTile()){
		var newDir:Vector2 = GetComponent(InfluencePathing).getInfluenceDirection();
		if(newDir.x>newDir.y){
			newDir.y *=-1;
		}else{
			newDir.x *=-1;
		}
		speed = new Vector3(newDir.x,0.0,newDir.y);
		speed.Normalize();
		speed *=randSpeed;
	}
	
}
function die(){
	//scream.Play();
	//Debug.Log("wää");
}


